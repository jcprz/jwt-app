package driver

import (
	"database/sql"
	"fmt"
	"log"
	"os"
)

var db *sql.DB

func ConnectDB() *sql.DB {
	//pgUrl, err := pq.ParseURL(os.Getenv("ELEPHANT_SQL_URL"))
	//if err != nil {
	//	log.Fatal(err)
	//}
	pgHost := os.Getenv("POSTGRES_HOST")
	pgUser := os.Getenv("POSTGRES_USER")
	pgPass := os.Getenv("POSTGRES_PASSWORD")
	pgPort := os.Getenv("POSTGRES_PORT")
	pgDbName := os.Getenv("POSTGRES_DBNAME")

	//pgToStr := fmt.Sprintf("host=localhost port=5432 user=postgres password=password dbname=postgres sslmode=disable")
	pgToStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", pgHost, pgPort, pgUser, pgPass, pgDbName)

	fmt.Println("DB Details to string: ", pgToStr)

	db, _ = sql.Open("postgres", pgToStr)
	//if err != nil {
	//	log.Fatal(err)
	//}

	er := db.Ping()
	if er != nil {
		log.Fatal(er)
	}

	return db
}
