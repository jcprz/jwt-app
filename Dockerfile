FROM golang:alpine

COPY . /usr/local/go/src/jwt-app
WORKDIR /usr/local/go/src/jwt-app

RUN go get . && \
    go build -o jwt-app

EXPOSE 9999/tcp

CMD ["./jwt-app"]