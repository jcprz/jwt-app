module github.com/jwt-app

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.2
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/subosito/gotenv v1.2.0
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
)
